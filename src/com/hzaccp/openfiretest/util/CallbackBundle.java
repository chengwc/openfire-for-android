package com.hzaccp.openfiretest.util;
//filename: CallbackBundle.java

import android.os.Bundle;
//简单的Bundle参数回调接口
public interface CallbackBundle {
	abstract void callback(Bundle bundle);
}
